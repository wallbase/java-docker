FROM openjdk:8-alpine

MAINTAINER  wangkun23 <845885222@qq.com>
# Default to UTF-8 file.encoding
ENV LANG C.UTF-8

# fixed TZ GMT
RUN echo https://mirrors.aliyun.com/alpine/v3.8/main > /etc/apk/repositories && \
    echo https://mirrors.aliyun.com/alpine/v3.8/community >> /etc/apk/repositories

RUN apk add -U tzdata ttf-dejavu fontconfig

RUN rm -rf /var/cache/apk/*
